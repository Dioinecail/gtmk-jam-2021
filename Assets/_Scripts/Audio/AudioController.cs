using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public DashBehaviour dashBehaviour;
    public ShootBehaviour shootBehaviour;
    public AnimationEventSender animationEvents;
    public JumpBehaviour jumpBehaviour;

    public StudioEventEmitter dashSound;
    public StudioEventEmitter shootSound;
    public StudioEventEmitter stepSound;
    public StudioEventEmitter jumpSound;



    private void OnDash()
    {
        dashSound.Play();
    }

    private void OnStep()
    {
        stepSound.Play();
    }

    private void OnJump()
    {
        jumpSound.Play();
    }

    private void OnShoot()
    {
        shootSound.Play();
    }

    private void OnEnable()
    {
        dashBehaviour.onDash += OnDash;
        shootBehaviour.onEndShootEvent += OnShoot;
        jumpBehaviour.onJump += OnJump;
        animationEvents.onStep += OnStep;
    }

    private void OnDisable()
    {
        dashBehaviour.onDash -= OnDash;
        shootBehaviour.onEndShootEvent -= OnShoot;
        jumpBehaviour.onJump -= OnJump;
        animationEvents.onStep -= OnStep;
    }
}
