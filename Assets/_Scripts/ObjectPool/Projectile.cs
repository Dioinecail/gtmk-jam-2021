﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PoolableBase
{
    // referense;
    public new BoxCollider collider;
    public GameObject explosionParticlePrefab;
    public GameObject particlesTrailPrefab;
    public TrailRenderer trail;
    public StudioEventEmitter hitSound;

    // parameters
    public LayerMask collisionMask;
    public float impactShakeAmp = 0.1f;
    public float impactShakeDuration = 0.1f;
    public float decelerationRate = 0;
    public bool returnToPoolOnImpact;

    // cache
    protected Vector3 direction;
    protected float damage;
    protected Collider[] collisionBuffer = new Collider[1];
    protected bool isFired;



    private void Update()
    {
        if (!isFired)
            return;

        CheckCollision();
        ParticleEmitter.EmitParticle(particlesTrailPrefab, transform.position);

        direction = Vector3.MoveTowards(direction, Vector3.zero, decelerationRate * Time.deltaTime);
        transform.position += direction * Time.deltaTime;
    }

    public void Reset()
    {
        trail.Clear();
        isFired = false;
        direction = Vector3.zero;
        damage = 0;
    }

    public void Init(Vector3 direction, float damage)
    {
        this.direction = direction;
        this.damage = damage;
        isFired = true;
    }

    protected void CheckCollision()
    {
        collisionBuffer[0] = null;

        Physics.OverlapBoxNonAlloc(transform.position, collider.size / 2, collisionBuffer, transform.rotation, collisionMask);

        if(collisionBuffer[0] != null)
        {
            // do action
            IKillable hitObject = collisionBuffer[0].GetComponent<IKillable>();

            if (hitObject != null)
                hitObject.RecieveDamage(damage, direction);

            // emit particle
            ParticleEmitter.EmitParticle(explosionParticlePrefab, transform.position);
            hitSound.Play();

            // return to pool
            CameraFollower.Instance.Shake(impactShakeAmp, impactShakeDuration);

            if (returnToPoolOnImpact)
                ReturnToPool();
            else
                Reset();
        }
    }
}