﻿using UnityEngine;

public delegate void OnReturnToPool(IPoolableObject sender);

public interface IPoolableObject
{
    event OnReturnToPool onReturnToPool;

    void ReturnToPool();
}