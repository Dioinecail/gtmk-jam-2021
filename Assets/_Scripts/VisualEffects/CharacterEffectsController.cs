using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEffectsController : MonoBehaviour
{
    public JumpBehaviour jumpBeh;
    public AnimationEventSender animEvents;

    public Transform leftFoot;
    public Transform rightFoot;
    public GameObject particleStep;
    public GameObject particleJump;



    private void OnJump()
    {
        ParticleEmitter.EmitParticle(particleStep, leftFoot.position);
    }

    private void OnLeftStep()
    {
        ParticleEmitter.EmitParticle(particleStep, leftFoot.position);
    }

    private void OnRightStep()
    {
        ParticleEmitter.EmitParticle(particleStep, rightFoot.position);
    }

    private void OnEnable()
    {
        jumpBeh.onJump += OnJump;
        animEvents.onLeftStep += OnLeftStep;    
    }

    private void OnDisable()
    {
        jumpBeh.onJump -= OnJump;
        animEvents.onRightStep -= OnRightStep;
    }
}