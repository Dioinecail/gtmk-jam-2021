﻿using System.Collections.Generic;
using UnityEngine;

public class ParticleEmitter
{
    private static Dictionary<GameObject, ParticleSystem> particleDictionary = new Dictionary<GameObject, ParticleSystem>();

    public static void EmitParticle(GameObject prefab, Vector3 position)
    {
        if(particleDictionary.ContainsKey(prefab))
        {
            ParticleSystem particle = particleDictionary[prefab];

            particle.gameObject.SetActive(true);
            particle.transform.position = position;
            particle.Play();
        }
        else
        {
            GameObject clone = Object.Instantiate(prefab, position, Quaternion.identity);
            ParticleSystem newSystem = clone.GetComponent<ParticleSystem>();

            newSystem.transform.position = position;
            newSystem.Play();
            particleDictionary.Add(prefab, newSystem);
        }
    }

    public static void Clear()
    {
        particleDictionary.Clear();
    }
}