using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PostProcessController : MonoBehaviour
{
    public ShootBehaviour shootBehaviour;
    public Volume postProcessDefault;
    public Volume postProcessSlowMo;

    public float transitionDuration;

    private Coroutine coroutine_Transition;



    public void TransitionToSlowMo()
    {
        if (coroutine_Transition != null)
            StopCoroutine(coroutine_Transition);

        coroutine_Transition = StartCoroutine(CoroutineTransition(1, transitionDuration));
    }

    public void TransitionToDefault()
    {
        if (coroutine_Transition != null)
            StopCoroutine(coroutine_Transition);

        coroutine_Transition = StartCoroutine(CoroutineTransition(0, transitionDuration));
    }

    private IEnumerator CoroutineTransition(float target, float duration)
    {
        float current = postProcessSlowMo.weight;

        float timer = 0;

        while(timer < duration)
        {
            timer += Time.deltaTime;

            postProcessSlowMo.weight = Mathf.Lerp(current, target, timer / duration);

            yield return null;
        }

        postProcessSlowMo.weight = target;
    }

    private void OnEnable()
    {
        shootBehaviour.onBeginShootEvent += TransitionToSlowMo;
        shootBehaviour.onEndShootEvent += TransitionToDefault;
    }

    private void OnDisable()
    {
        shootBehaviour.onBeginShootEvent -= TransitionToSlowMo;
        shootBehaviour.onEndShootEvent -= TransitionToDefault;
    }
}
