﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [Header("Main Refs")]
    public Animator anima;
    public Animator bowAnima;
    public Rigidbody rBody;
    public Transform bodyTransform;
    public Transform bodyProxyTransform;
    public Transform modelRoot;

    [Header("Behaviour Refs")]
    public MoveBehaviour moveBehaviour;
    public CollisionBehaviour collisionBehaviour;
    public JumpBehaviour jumpBehaviour;
    public ShootBehaviour shootBehaviour;
    public DashBehaviour dashBehaviour;
    public PlayerBehaviour playerBehaviour;

    [Header("IK Parameters")]
    [Range(0, 1f)]
    public float bodyMaxWeight;

    private float velocityX;
    private float velocityY;
    private bool onGround;
    private bool onWall;

    private float currentBodyIKWeight;
    private Vector3 aimPosition;



    private void Update()
    {
        UpdateVelocity();
        UpdateCollisionStates();
        UpdateAnimatorValues();
    }

    private void LateUpdate()
    {
        ApplyIK();
    }

    private void UpdateVelocity()
    {
        velocityX = Mathf.Abs(rBody.velocity.x);
        velocityY = rBody.velocity.y;
    }

    private void UpdateCollisionStates()
    {
        onGround = collisionBehaviour.onGround;
        onWall = collisionBehaviour.onWall;
    }

    private void UpdateAnimatorValues()
    {
        anima.SetFloat("VelocityX", velocityX);
        anima.SetBool("OnGround", onGround);
    }

    private void OnJumpEvent()
    {
        anima.Play("Jump");
    }

    private void OnBeginShoot()
    {
        bowAnima.SetBool("Aiming", true);
        currentBodyIKWeight = 1;
    }

    private void OnEndShoot()
    {
        bowAnima.SetBool("Aiming", false);
        currentBodyIKWeight = 0;
    }

    private void OnAim(Vector3 aimPosition)
    {
        this.aimPosition = aimPosition;
    }

    private void OnDash()
    {
        anima.Play("Roll");
    }

    private void OnPlayerDied()
    {
        anima.Play("Death");
    }

    private void ApplyIK()
    {
        anima.SetLayerWeight(1, currentBodyIKWeight);

        Vector3 proxyDirection = aimPosition - bodyProxyTransform.position;
        Quaternion proxyRotation = Quaternion.LookRotation(proxyDirection, Vector3.up);
        // set body proxy transform to look at gun transform;
        bodyProxyTransform.rotation = proxyRotation;
        // set body transform to copy a child of proxy transform;
        bodyTransform.rotation = Quaternion.Lerp(bodyTransform.rotation, bodyProxyTransform.GetChild(0).rotation, currentBodyIKWeight * bodyMaxWeight);

        if (Mathf.Abs(rBody.velocity.x) > 0.01f)
        {
           float yAngle = rBody.velocity.x > 0 ? 90 : -90;

            if (currentBodyIKWeight > 0)
                yAngle = proxyDirection.x > 0 ? 90 : -90;

            Quaternion targetRotation = Quaternion.Euler(0, yAngle, 0);
            modelRoot.rotation = Quaternion.Lerp(modelRoot.rotation, transform.rotation * targetRotation, 1);
        }
    }

    private void OnEnable()
    {
        jumpBehaviour.onJump += OnJumpEvent;
        shootBehaviour.onBeginShootEvent += OnBeginShoot;
        shootBehaviour.onEndShootEvent += OnEndShoot;
        shootBehaviour.onAimEvent += OnAim;
        dashBehaviour.onDash += OnDash;
        playerBehaviour.onPlayerDied += OnPlayerDied;
    }

    private void OnDisable()
    {
        jumpBehaviour.onJump -= OnJumpEvent;
        shootBehaviour.onBeginShootEvent -= OnBeginShoot;
        shootBehaviour.onEndShootEvent -= OnEndShoot;
        shootBehaviour.onAimEvent -= OnAim;
        dashBehaviour.onDash -= OnDash;
        playerBehaviour.onPlayerDied -= OnPlayerDied;
    }
}