﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventSender : MonoBehaviour
{
    public event Action onStep;
    public event Action onLeftStep;
    public event Action onRightStep;



    public void OnLeftStep()
    {
        onStep?.Invoke();
        onLeftStep?.Invoke();
    }

    public void OnRightStep()
    {
        onStep?.Invoke();
        onRightStep?.Invoke();
    }
}