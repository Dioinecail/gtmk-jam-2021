﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKController : MonoBehaviour
{
    // referenses
    public Transform target;
    public Animator anima;

    // parameter
    public bool isActive;
    [Range(0, 1)]
    public float weight;
    public AvatarIKGoal type;



    public void Enable(bool state)
    {
        isActive = state;
    }

    private void OnAnimatorIK(int layerIndex)
    {
        ApplyIK();
    }

    private void ApplyIK()
    {
        anima.SetIKPosition(type, target.position);
        anima.SetIKPositionWeight(type, weight);
    }
}