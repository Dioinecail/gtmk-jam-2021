using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public PlayerBehaviour playerBehaviour;
    public EnemyManager enemyManager;
    public LevelManager levelManager;
    public TMP_Text levelsInfoText;
    public TMP_Text levelsInfoOnDeathText;
    public PlayableDirector timelineOnPlayerDeath;
    public CanvasGroup uiOnDeath;

    public string levelsInfoFormat;
    public string levelsInfoOnDeathFormat;
    public float initialStartDelay;
    public float restartDelay;

    private int levelsBeaten;



    public void Reload()
    {
        GameobjectPoolSystem.Clear();
        ParticleEmitter.Clear();
        SceneManager.LoadScene(0);
    }

    public void OpenLink(string link)
    {
        Application.OpenURL(link);
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Invoke("StartLevel", initialStartDelay);
        uiOnDeath.interactable = false;
        uiOnDeath.blocksRaycasts = false;
    }

    private void OnAllEnemiesDied()
    {
        levelsBeaten++;
        levelsInfoText.text = string.Format(levelsInfoFormat, levelsBeaten + 1);
        levelsInfoOnDeathText.text = string.Format(levelsInfoOnDeathFormat, levelsBeaten);
        Invoke("StartLevel", restartDelay);
    }

    private void StartLevel()
    {
        levelManager.CreateNewLevel();
        uiOnDeath.interactable = false;
        uiOnDeath.blocksRaycasts = false;
    }

    private void OnPlayerDied()
    {
        timelineOnPlayerDeath.Play();
        timelineOnPlayerDeath.stopped += TimelineOnPlayerDeathStopped;
        uiOnDeath.interactable = true;
        uiOnDeath.blocksRaycasts = true;
    }

    private void TimelineOnPlayerDeathStopped(PlayableDirector obj)
    {
        obj.stopped -= TimelineOnPlayerDeathStopped;
    }

    private void OnEnable()
    {
        enemyManager.onAllEnemiesDied += OnAllEnemiesDied;
        playerBehaviour.onPlayerDied += OnPlayerDied;
    }

    private void OnDisable()
    {
        enemyManager.onAllEnemiesDied -= OnAllEnemiesDied;
        playerBehaviour.onPlayerDied -= OnPlayerDied;
    }
}