﻿using UnityEngine;

public interface IKillable
{
    float HP { get;}

    void RecieveDamage(float damage, Vector3 direction);
}