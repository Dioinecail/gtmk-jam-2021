﻿using UnityEngine;

public class MoveBehaviour : AbstractBehaviour
{
    // referenses
    public ShootBehaviour shootBehaviour;
    public AmmoBehaviour ammoBehaviour;

    // parameters
    public float moveSpeed = 5;
    public float CurrentMult { get; set; } = -1;
    public float pullForcePerDistance;

    // cache
    public float MoveDirection { get; private set; }
    private Transform[] pullArrows;



    protected override void Awake()
    {
        base.Awake();
        pullArrows = new Transform[ammoBehaviour.ammoObjects.Length];
    }

    private void Update()
    {
        GetInput();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void GetInput()
    {
        MoveDirection = Input.GetAxis("Horizontal");
    }

    private void Move()
    {
        float movementSpeed = CurrentMult > 0 
            ? CurrentMult * Mathf.Sign(MoveDirection) 
            : moveSpeed * MoveDirection;

        Vector3 moveDirection = new Vector3(movementSpeed, rBody.velocity.y);
        Vector3 totalPullForce = Vector3.zero;

        int totalPullingArrows = 0;

        for (int i = 0; i < pullArrows.Length; i++)
        {
            if(pullArrows[i] != null)
            {
                totalPullingArrows++;
                Vector3 pullForce = pullArrows[i].position - PlayerBehaviour.center.position;
                totalPullForce += pullForce * pullForcePerDistance;
            }
        }

        if(totalPullingArrows > 0)
        {
            totalPullForce /= totalPullingArrows;
        }

        rBody.velocity = moveDirection + totalPullForce;
    }

    private void OnProjectileShot(Transform target)
    {
        ConnectedProjectile projectile = target.GetComponent<ConnectedProjectile>();

        for (int i = 0; i < pullArrows.Length; i++)
        {
            if (pullArrows[i] == null)
            {
                pullArrows[i] = target;
                projectile.onReturnToPool += OnProjectileReturnedToPool;
            }
        }
    }

    private void OnProjectileReturnedToPool(IPoolableObject sender)
    {
        sender.onReturnToPool -= OnProjectileReturnedToPool;

        for (int i = 0; i < pullArrows.Length; i++)
        {
            if (pullArrows[i] != null)
            {
                pullArrows[i] = null;
                break;
            }
        }
    }

    private void OnEnable()
    {
        shootBehaviour.onShootProjectileEvent += OnProjectileShot;
    }

    private void OnDisable()
    {
        shootBehaviour.onShootProjectileEvent -= OnProjectileShot;
    }
}
