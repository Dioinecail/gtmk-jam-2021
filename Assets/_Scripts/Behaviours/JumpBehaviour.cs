﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour
{
    public event Action onJump;

    //public AmmoBehaviour ammoBehaviour;

    public float JumpSpeed = 5.5f;
    public float jumpMultiplier = 1;
    private bool secondJump;



    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (collisionBehaviour.onGround)
            {
                OnJump();
                secondJump = true;
            }
            else if (secondJump)
            {
                OnJump();
                secondJump = false;
            }
        }
    }

    private void OnJump()
    {
        onJump?.Invoke();
        rBody.isKinematic = false;
        rBody.velocity = new Vector3(rBody.velocity.x, JumpSpeed * jumpMultiplier);
    }

    //private void SecondJump()
    //{
    //    if(ammoBehaviour.CurrentAmmo > 0)
    //    {
    //        ammoBehaviour.RemoveAmmo();

    //        OnJump();
    //    }
    //}
}
