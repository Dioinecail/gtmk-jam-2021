﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBehaviour : AbstractBehaviour
{
    public ShootBehaviour shootBehaviour;
    public GameObject[] ammoObjects;

    public float ammoPullStrengthRate;
    public float pullThreshold;
    public int CurrentAmmo { get; private set; }

    private bool isPulling;
    private List<Transform> availableProjectiles;
    private Coroutine coroutine_Pull;
    private float currentAmmoPullStrength;



    public void RemoveAmmo()
    {
        if (CurrentAmmo <= 0)
            return;

        CurrentAmmo--;

        ammoObjects[CurrentAmmo].SetActive(false);
    }

    private void Start()
    {
        CurrentAmmo = ammoObjects.Length;

        availableProjectiles = new List<Transform>();
    }

    private void Update()
    {
        if(CurrentAmmo < ammoObjects.Length)
        {
            if(CurrentAmmo == 0)
            {
                if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.R))
                {
                    OnBeginPull();
                }

                if ((Input.GetButtonUp("Fire1") || Input.GetKeyUp(KeyCode.R)) && isPulling)
                {
                    OnEndPull();
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    OnBeginPull();
                }

                if (Input.GetKeyUp(KeyCode.R) && isPulling)
                {
                    OnEndPull();
                }
            }
        }
    }

    private void AddAmmo()
    {
        if (CurrentAmmo >= ammoObjects.Length)
            return;

        ammoObjects[CurrentAmmo].SetActive(true);

        CurrentAmmo++;
    }

    private void OnBeginPull()
    {
        isPulling = true;

        if (coroutine_Pull != null)
            StopCoroutine(coroutine_Pull);

        coroutine_Pull = StartCoroutine(CoroutinePullAmmo());
    }

    private void OnEndPull()
    {
        isPulling = false;

        if (coroutine_Pull != null)
            StopCoroutine(coroutine_Pull);
    }

    private IEnumerator CoroutinePullAmmo()
    {
        currentAmmoPullStrength = 0;

        while(availableProjectiles.Count > 0)
        {
            for (int i = 0; i < availableProjectiles.Count; i++)
            {
                Projectile projectile = availableProjectiles[i].GetComponent<Projectile>();
                projectile.Reset();

                Vector3 direction = PlayerBehaviour.center.position - availableProjectiles[i].position;
                direction.z = 0;
                direction.Normalize();

                currentAmmoPullStrength += ammoPullStrengthRate * Time.deltaTime;

                availableProjectiles[i].position += direction * currentAmmoPullStrength * Time.deltaTime;

                if(Vector3.Distance(availableProjectiles[i].position, PlayerBehaviour.center.position) < pullThreshold)
                {
                    projectile.ReturnToPool();
                    availableProjectiles.RemoveAt(i);
                    AddAmmo();
                    break;
                }
            }

            yield return null;
        }

        coroutine_Pull = null;
    }

    private void OnProjectileShot(Transform target)
    {
        availableProjectiles.Add(target);
    }

    private void OnEnable()
    {
        shootBehaviour.onShootProjectileEvent += OnProjectileShot;
    }

    private void OnDisable()
    {
        shootBehaviour.onShootProjectileEvent -= OnProjectileShot;
    }
}