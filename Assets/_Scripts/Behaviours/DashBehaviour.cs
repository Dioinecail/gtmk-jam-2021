using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashBehaviour : AbstractBehaviour
{
    public event Action onDash;

    public MoveBehaviour moveBehaviour;
    //public AmmoBehaviour ammoBehaviour;
    public TrailRenderer dashTrail;

    public float dashSpeedMult = 4;
    public float dashTime = 0.25f;

    private Coroutine coroutine_Dash;
    private bool dashAvailable;



    private void Update()
    {
        //if(Input.GetButtonDown("Fire2") && ammoBehaviour.CurrentAmmo > 0)
        //{
        //    OnDash();
        //}
        if (collisionBehaviour.onGround)
        {
            dashAvailable = true;
        }

        if (Input.GetButtonDown("Fire2"))
        {
            if(dashAvailable)
            {
                dashAvailable = false;
                OnDash();
            }
        }
    }

    private void OnDash()
    {
        if (coroutine_Dash != null)
            return;

        //ammoBehaviour.RemoveAmmo();

        onDash?.Invoke();
        coroutine_Dash = StartCoroutine(CoroutineDash());
    }

    private IEnumerator CoroutineDash()
    {
        moveBehaviour.CurrentMult = dashSpeedMult;
        dashTrail.Clear();
        dashTrail.time = 1f;
        dashTrail.gameObject.SetActive(true);

        yield return new WaitForSeconds(dashTime);

        moveBehaviour.CurrentMult = -1;
        dashTrail.time = 0;
        dashTrail.gameObject.SetActive(false);

        coroutine_Dash = null;
    }
}