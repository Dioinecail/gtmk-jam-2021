﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootBehaviour : AbstractBehaviour
{
    // events
    public event Action onBeginShootEvent;
    public event Action onEndShootEvent;
    public event Action<Transform> onShootProjectileEvent;
    public event Action<Vector3> onAimEvent;

    public UnityEvent onBeginShoot;
    public UnityEvent onShotCharged;
    public UnityEvent onEndShoot;

    // referenses
    public GameObject projectilePrefab;
    public Transform gunTransform;
    public AmmoBehaviour ammoBehaviour;

    // parameters
    public float aimSpeed = 5f;
    public float shootOffset = 1;
    public float ammoSpeed = 1;
    public float damage = 20;
    public float fireCooldown = 0.5f; 
    public float chargeTimePerShot;
    public int maxShots;
    public float chargedShotPositionOffset;
    public Vector3 aimingStartPosition;

    // cache
    private Vector3 aimPosition;
    private Vector3 aimDirection;
    private Quaternion targetRotation;
    private Coroutine coroutine_Cooldown;
    private bool isAiming;



    private void Update()
    {
        Aim();

        if (Input.GetButtonDown("Fire1") && ammoBehaviour.CurrentAmmo > 0)
        {
            OnBeginShoot();
        }

        if (Input.GetButtonUp("Fire1") && isAiming)
        {
            OnShoot();
        }
    }

    private void OnBeginShoot()
    {
        if (coroutine_Cooldown != null)
                return;

        isAiming = true;

        onBeginShoot?.Invoke();
        onBeginShootEvent?.Invoke();
    }

    private void OnShoot()
    {
        coroutine_Cooldown = StartCoroutine(CoroutineCooldown());

        ammoBehaviour.RemoveAmmo();

        GameObject po = GameobjectPoolSystem.Instantiate(projectilePrefab, aimPosition, targetRotation);
        Projectile projectile = po.GetComponent<Projectile>();
        projectile.Reset();
        projectile.Init(aimDirection * ammoSpeed, damage);

        onEndShoot?.Invoke();
        onEndShootEvent?.Invoke();
        onShootProjectileEvent?.Invoke(po.transform);
        isAiming = false;
    }

    private void Aim()
    {
        // find the target position that the player is looking at
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 mousePosition = mouseRay.origin + mouseRay.direction * -Camera.main.transform.position.z;

        aimDirection = mousePosition - (transform.position + aimingStartPosition);
        aimDirection.z = 0;
        aimDirection.Normalize();

        aimPosition = transform.position + (aimingStartPosition + aimDirection * shootOffset);

        targetRotation = Quaternion.LookRotation(aimDirection, Vector3.up);
        gunTransform.position = aimPosition;
        gunTransform.rotation = targetRotation;

        onAimEvent?.Invoke(gunTransform.position);
    }

    private IEnumerator CoroutineCooldown()
    {
        yield return new WaitForSeconds(fireCooldown);

        coroutine_Cooldown = null;
    }

    private void OnDrawGizmos()
    {
        if (aimDirection.magnitude > 0.1f && aimPosition.magnitude > 0.1f)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position + aimingStartPosition, transform.position + aimingStartPosition + aimDirection * shootOffset);
        }
    }
}