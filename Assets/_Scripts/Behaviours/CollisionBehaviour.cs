﻿using System.Collections;
using UnityEngine;

public class CollisionBehaviour : MonoBehaviour
{
    // referenses
    public JumpBehaviour jumpBehaviour;

    // paremeters
    public Vector2[] StickToWallOffset;
    public float handRadius = 0.25f;

    public Vector2 feetOffset = new Vector2(0, 0.06f);
    public float FeetRadius = 0.18f;

    public float jumpDelay;

    public bool onGround;
    public bool onLeftWall;
    public bool onRightWall;
    public bool onWall
    {
        get
        {
            return onLeftWall || onRightWall;
        }
    }

    public LayerMask GroundMask;
    public Quaternion angleUnderFeet;

    // cache
    private Collider[] groundBuffer = new Collider[4];
    private RaycastHit[][] wallBuffer = new RaycastHit[4][];
    private Coroutine coroutine_JumpCooldown;



    private void Update()
    {
        ClearBuffers();

        onLeftWall = false;
        onRightWall = false;

        Physics.OverlapSphereNonAlloc((Vector2)transform.position + feetOffset, FeetRadius, groundBuffer, GroundMask);

        for (int i = 0; i < StickToWallOffset.Length; i++)
        {
            onRightWall |= Physics.Raycast((Vector2)transform.position + StickToWallOffset[i], Vector3.right, handRadius, GroundMask);
            onLeftWall |= Physics.Raycast((Vector2)transform.position + StickToWallOffset[i], Vector3.left, handRadius, GroundMask);
        }

        if (coroutine_JumpCooldown != null)
            return;

        onGround = groundBuffer[0] != null;
    }

    private void ClearBuffers()
    {
        for (int i = 0; i < groundBuffer.Length; i++)
        {
            groundBuffer[i] = null;
        }

        for (int i = 0; i < wallBuffer.GetLength(0); i++)
        {
            wallBuffer[i] = new RaycastHit[1];
        }
    }

    private void OnJump()
    {
        coroutine_JumpCooldown = StartCoroutine(CoroutineJumpCooldown());
    }

    private IEnumerator CoroutineJumpCooldown()
    {
        yield return new WaitForSeconds(jumpDelay);

        coroutine_JumpCooldown = null;
    }

    private void OnEnable()
    {
        jumpBehaviour.onJump += OnJump;
    }

    private void OnDisable()
    {
        jumpBehaviour.onJump -= OnJump;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + feetOffset, FeetRadius);
        Gizmos.color = Color.yellow;

        foreach (var item in StickToWallOffset)
        {
            Gizmos.DrawWireSphere((Vector2)transform.position + item, handRadius);
        }
    }
}
