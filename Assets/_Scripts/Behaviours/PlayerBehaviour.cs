﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : AbstractBehaviour, IKillable
{
    public event Action onPlayerDied;

    public float knockBack; 
    public float maxHealth;

    public float HP { get; private set; }
    public static Transform center;
    public Transform _center;

    private bool isDead;



    public void RecieveDamage(float damage, Vector3 direction)
    {
        direction.Normalize();
        direction *= knockBack;

        rBody.MovePosition(rBody.position + direction);

        HP -= damage;

        if(HP <= 0 && !isDead)
        {
            OnDie();
        }
    }

    private void Start()
    {
        HP = maxHealth;
        center = _center;   
    }

    private void OnDie()
    {
        isDead = true;

        AbstractBehaviour[] allBehaviours = GetComponents<AbstractBehaviour>();

        for (int i = 0; i < allBehaviours.Length; i++)
        {
            allBehaviours[i].enabled = false;
        }

        onPlayerDied?.Invoke();
    }
}
