using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileConnectionBehaviour : AbstractBehaviour
{
    public ShootBehaviour shootBehaviour;
    public AmmoBehaviour ammoBehaviour;
    public LineRenderer[] ropeRenderers;
    public Material ropeMaterial;

    public int segmentsCount;
    public float segmentLength;

    private RopeBridge[] rope;



    private void Start()
    {
        rope = new RopeBridge[ammoBehaviour.ammoObjects.Length];

        for (int i = 0; i < rope.Length; i++)
        {
            rope[i] = new RopeBridge();
        }
    }

    private void Update()
    {
        if(rope != null)
        {
            for (int i = 0; i < rope.Length; i++)
            {
                if(rope[i].IsActive)
                {
                    Vector3[] ropeSegments = rope[i].GetRopePositions();
                    ropeRenderers[i].positionCount = ropeSegments.Length;
                    ropeRenderers[i].SetPositions(ropeSegments);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (rope != null)
        {
            for (int i = 0; i < rope.Length; i++)
            {
                if(rope[i].IsActive)
                    rope[i].Simulate();
            }
        }

        ropeMaterial.SetVector("_startPosition", PlayerBehaviour.center.position);
    }

    private void OnProjectileShot(Transform target)
    {
        ConnectedProjectile projectile = target.GetComponent<ConnectedProjectile>();

        for (int i = 0; i < rope.Length; i++)
        {
            if(!rope[i].IsActive)
            {
                rope[i].InitRope(PlayerBehaviour.center, target, segmentsCount, segmentLength);
                projectile.onReturnToPool += OnProjectileReturnedToPool;
                break;
            }
        }
    }

    private void OnProjectileReturnedToPool(IPoolableObject sender)
    {
        sender.onReturnToPool -= OnProjectileReturnedToPool;

        for (int i = 0; i < rope.Length; i++)
        {
            if (rope[i].IsActive)
            {
                rope[i].Reset();
                ropeRenderers[i].positionCount = 0;
                break;
            }
        }
    }

    private void OnEnable()
    {
        shootBehaviour.onShootProjectileEvent += OnProjectileShot;    
    }

    private void OnDisable()
    {
        shootBehaviour.onShootProjectileEvent -= OnProjectileShot;
    }
}
