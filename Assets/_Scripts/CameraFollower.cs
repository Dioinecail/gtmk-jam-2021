﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour, IEquatable<float>
{
    public static CameraFollower Instance;

    // referenses
    public Transform target;

    // parameters
    public float CameraSpeed = 5;
    public float mouseLookDistance = 1;
    public Vector2 Offset;

    // cache
    private Camera mainCam;
    private Vector2 targetOffset;
    private Vector2 shakeOffset;
    private Vector2 CurrentOffset {  get { return targetOffset + shakeOffset; } }
    private Ray mouseRay;

    private Coroutine coroutine_Shake;



    public void Shake(float amp, float duration)
    {
        if (coroutine_Shake != null)
            StopCoroutine(coroutine_Shake);

        coroutine_Shake = StartCoroutine(CoroutineShake(amp, duration));
    }

    private void Awake()
    {
        mainCam = GetComponent<Camera>();
        Instance = this;
    }

    private void FixedUpdate()
    {
        if(target != null)
        {
            FollowTarget();
            ExtendCameraTowardsCursor();
        }
    }

    private void FollowTarget()
    {
        Vector3 targetPosition = target.position;
        targetPosition.z = transform.position.z;
        targetPosition += (Vector3)(Offset + CurrentOffset * mouseLookDistance);
        transform.position = Vector3.Lerp(transform.position, targetPosition, CameraSpeed * Time.fixedDeltaTime);
    }

    private void ExtendCameraTowardsCursor()
    {
        mouseRay = mainCam.ScreenPointToRay(Input.mousePosition);
        targetOffset = new Vector2(mouseRay.direction.x, mouseRay.direction.y);
    }

    private IEnumerator CoroutineShake(float amp, float duration)
    {
        float timer = 0;

        while(timer < duration)
        {
            timer += Time.deltaTime;

            shakeOffset = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)) * Vector2.up * amp;

            yield return null;
        }

        shakeOffset = Vector2.zero;

        coroutine_Shake = null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(mouseRay.origin, mouseRay.origin + mouseRay.direction);
    }

    public bool Equals(float other)
    {
        throw new NotImplementedException();
    }
}