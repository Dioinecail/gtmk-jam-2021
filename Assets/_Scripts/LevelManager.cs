using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LevelManager : MonoBehaviour
{
    public event Action<Transform> onLevelCreated;

    public GameObject[] possibleLevels;

    public Transform levelRoot;

    public PlayableDirector timelineLevelTransitionStart;
    public PlayableDirector timelineLevelTransitionEnd;



    public void CreateNewLevel()
    {
        timelineLevelTransitionStart.Play();
        timelineLevelTransitionStart.stopped += OnTransitionStartFinished;
    }

    private void CreateLevel()
    {
        Transform level = Instantiate(possibleLevels.GetRandom(), levelRoot).transform;
        if (UnityEngine.Random.Range(0, 1f) < 0.5f)
            level.localRotation = Quaternion.Euler(0, 180, 0);

        onLevelCreated?.Invoke(level);
    }

    private void OnTransitionStartFinished(PlayableDirector obj)
    {
        obj.stopped -= OnTransitionStartFinished;
        ClearLevel();
        CreateLevel();

        timelineLevelTransitionEnd.Play();
    }

    private void ClearLevel()
    {
        Destroy(levelRoot.GetChild(0).gameObject);
    }
}
