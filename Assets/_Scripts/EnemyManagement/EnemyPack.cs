using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyPack : ScriptableObject
{
    public EnemyBase[] possibleEnemies;

    public int initialEnemyCount;
    public int maxEnemiesAlive;
    public int enemyTotalCount;
    public float spawnDelay;
}