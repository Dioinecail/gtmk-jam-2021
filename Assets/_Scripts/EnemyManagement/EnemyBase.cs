using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EnemyBase : PoolableBase, IKillable
{
    public event Action<EnemyBase> onDieEvent;

    public GameObject projectilePrefab;

    public float movementSpeed;
    public float maxHealth;
    public float HP { get; private set; }
    public float knockBack;
    public float projectileSpeed;
    public float fireDistance;
    public float fireCooldown;
    public float fireCooldownVariance;
    public float damage;
    public float minApproachDistance;
    public float minApproachDistanceVariance;

    public LayerMask obstacleMask;

    public UnityEvent onHit;
    public UnityEvent onSpawn;
    public UnityEvent onDie;

    protected Transform target;
    protected Coroutine coroutine_Shoot;



    public virtual void Init()
    {
        target = PlayerBehaviour.center;
        HP = maxHealth;

        if (coroutine_Shoot != null)
            StopCoroutine(coroutine_Shoot);

        coroutine_Shoot = StartCoroutine(CoroutineShoot());

        onSpawn?.Invoke();
    }

    public void RecieveDamage(float damage, Vector3 direction)
    {
        direction.Normalize();
        direction *= knockBack;

        transform.position += direction;

        HP -= damage;

        if (HP <= 0)
            OnDie();

        onHit?.Invoke();
    }

    protected virtual void Shoot()
    {
        Vector3 position = transform.position;
        Quaternion rotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);

        Projectile newProjectile = GameobjectPoolSystem.Instantiate(projectilePrefab, position, rotation).GetComponent<Projectile>();
        newProjectile.Reset();
        newProjectile.Init(rotation * Vector3.forward * projectileSpeed, damage);
    }

    protected IEnumerator CoroutineShoot()
    {
        float cooldown = fireCooldown + UnityEngine.Random.Range(-fireCooldownVariance, fireCooldownVariance);

        yield return new WaitForSeconds(cooldown);

        if (Vector3.Distance(target.position, transform.position) < fireDistance)
            Shoot();

        coroutine_Shoot = StartCoroutine(CoroutineShoot());
    }


    protected virtual void OnDie()
    {
        onDieEvent?.Invoke(this);
        onDie?.Invoke();
    }
}