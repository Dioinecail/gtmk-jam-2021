using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : EnemyBase
{
    public float verticalApproachOffset;
    public float approachOffsetDelay;

    private Coroutine coroutine_ApproachOffset;
    private Vector3 currentApproachOffset = Vector3.zero;
    private Vector3 targetPosition;
    private Vector3 direction;
    private Vector3 hitPoint;
    private Vector3 perpendicularVector;
    private Collider[] stuckInsideWallBuffer = new Collider[1];



    public override void Init()
    {
        base.Init();

        if (coroutine_ApproachOffset != null)
            StopCoroutine(coroutine_ApproachOffset);

        coroutine_ApproachOffset = StartCoroutine(CoroutineChangeApproachOffset());
    }

    private void Update()
    {
        if (target != null)
            FollowTarget();
    }

    private void FollowTarget()
    {
        direction = GetDirection();
        Vector3 newPosition = transform.position + direction * movementSpeed * Time.deltaTime;
        newPosition.z = 0;

        transform.position = newPosition;
    }

    private Vector3 GetDirection()
    {
        Vector3 vectorFromTarget = transform.position - target.position;
        vectorFromTarget.Normalize();
        Vector3 vectorFromTargetScaled = vectorFromTarget * minApproachDistance;
        Vector3 positionProjection = new Vector3(target.position.x + vectorFromTargetScaled.x, target.position.y + verticalApproachOffset);
        Vector3 randomOffset = currentApproachOffset;
        targetPosition = positionProjection + randomOffset;

        Vector3 raycastDirection = targetPosition - transform.position;

        float raycastDistance = raycastDirection.magnitude;
        raycastDirection.Normalize();

        Physics.Raycast(transform.position, raycastDirection, out RaycastHit hitInfo, raycastDistance, obstacleMask, QueryTriggerInteraction.Ignore);

        if(hitInfo.collider != null)
        {
            hitPoint = hitInfo.point;
            Vector3 directionToHitPoint = hitPoint - transform.position;
            directionToHitPoint.Normalize();

            Vector3 hitNormal = hitInfo.normal;
            hitNormal.Normalize();

            Vector3 cross = Vector3.Cross(directionToHitPoint, hitNormal);

            float angle = Mathf.Sign(cross.z) * 90;

            perpendicularVector = Quaternion.Euler(0, 0, angle) * directionToHitPoint;
            perpendicularVector.Normalize();
            targetPosition = hitPoint + perpendicularVector;
        }

        ClearStuckInsideWallBuffer();

        Physics.OverlapSphereNonAlloc(transform.position, 0.1f, stuckInsideWallBuffer, obstacleMask, QueryTriggerInteraction.Ignore);

        if(stuckInsideWallBuffer[0] != null)
        {
            targetPosition += Vector3.up * 0.1f;
        }

        Vector3 direction = targetPosition - transform.position;
        float distanceToTarget = direction.magnitude;
        distanceToTarget = Mathf.Clamp(distanceToTarget, 0, 1);

        direction.Normalize();
        direction *= distanceToTarget;

        return direction;
    }

    private void ClearStuckInsideWallBuffer()
    {
        stuckInsideWallBuffer[0] = null;
    }

    private IEnumerator CoroutineChangeApproachOffset()
    {
        yield return new WaitForSeconds(approachOffsetDelay);

        currentApproachOffset = Quaternion.Euler(0, 0, Random.Range(0, 360)) * Vector3.up * Random.Range(-minApproachDistanceVariance, minApproachDistanceVariance);

        coroutine_ApproachOffset = StartCoroutine(CoroutineChangeApproachOffset());
    }

    protected override void OnDie()
    {
        if (coroutine_Shoot != null)
            StopCoroutine(coroutine_Shoot);

        base.OnDie();
    }

    private void OnDrawGizmosSelected()
    {
        if (target == null)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, targetPosition);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + direction);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, hitPoint);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(hitPoint, hitPoint + perpendicularVector);
    }
}