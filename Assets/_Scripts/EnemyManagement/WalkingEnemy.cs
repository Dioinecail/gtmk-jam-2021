using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingEnemy : EnemyBase
{
    public Transform aimTransform;
    public Transform model;
    public Rigidbody rBody;

    public float raycastOffset = 0.1f;
    public float raycastHeight = 0.5f;

    private Vector3 directionVector = Vector3.right;



    protected override void Shoot()
    {
        Vector3 position = aimTransform.position;
        Quaternion rotation = Quaternion.LookRotation(target.position - aimTransform.position, Vector3.up);

        Projectile newProjectile = GameobjectPoolSystem.Instantiate(projectilePrefab, position, rotation).GetComponent<Projectile>();
        newProjectile.Reset();
        newProjectile.Init(rotation * Vector3.forward * projectileSpeed, damage);
    }

    private void Update()
    {
        if (target != null)
        {
            rBody.velocity = new Vector3(0, rBody.velocity.y);
            rBody.angularVelocity = Vector3.zero;
            FollowTarget();
        }
    }

    private void FollowTarget()
    {
        GetDirection();
        Vector3 newPosition = transform.position + directionVector * movementSpeed * Time.deltaTime;
        newPosition.z = 0;

        transform.position = newPosition;
        if (Mathf.Abs(directionVector.x) > 0.01f)
        {
            float yAngle = directionVector.x > 0 ? 90 : -90;

            Quaternion targetRotation = Quaternion.Euler(0, yAngle, 0);
            model.rotation = Quaternion.Lerp(model.rotation, transform.rotation * targetRotation, 1);
        }
    }

    private void GetDirection()
    {
        Vector3 raycastPosition = transform.position + new Vector3(Mathf.Sign(directionVector.x) * raycastOffset, raycastHeight);

        Physics.Raycast(raycastPosition, Vector3.down, out RaycastHit hitInto, raycastHeight * 2, obstacleMask, QueryTriggerInteraction.Ignore);

        Vector3 wallRaycastPosition = transform.position + Vector3.up * 0.1f;

        Physics.Raycast(wallRaycastPosition, directionVector, out RaycastHit wallHit, 0.2f, obstacleMask, QueryTriggerInteraction.Ignore);

        if(hitInto.collider == null || wallHit.collider != null)
        {
            directionVector = -directionVector;
        }
    }
}