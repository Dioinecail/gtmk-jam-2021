using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public event Action onAllEnemiesDied;

    public LevelManager levelManager;
    public Transform enemyRoot;
    public EnemyPack[] possibleEnemies;

    public LayerMask platformMask;
    public float raycastHeight = 0.25f;

    private int currentActiveEnemies;
    private int spawnedEnemiesTotal;
    private int enemiesKilled;
    private Vector2[] spawnPositions;
    private Transform levelRoot;

    private EnemyPack currentPack;
    private Coroutine coroutine_SpawnEnemies;



    public void StartSpawning()
    {
        currentPack = possibleEnemies.GetRandom();

        Stack<Vector2> positions = new Stack<Vector2>(spawnPositions);
        positions.Randomize();

        for (int i = 0; i < currentPack.initialEnemyCount; i++)
        {
            SpawnEnemy(positions.Pop());
        }

        coroutine_SpawnEnemies = StartCoroutine(CoroutineSpawnEnemies());
    }

    public void StopSpawning()
    {
        if (coroutine_SpawnEnemies != null)
            StopCoroutine(coroutine_SpawnEnemies);
    }

    private void OnLevelCreated(Transform levelRoot)
    {
        currentActiveEnemies = 0;
        spawnedEnemiesTotal = 0;
        enemiesKilled = 0;

        this.levelRoot = levelRoot;
        GetSpawnPositions();
        StartSpawning();
    }

    private void SpawnEnemy(Vector2 position)
    {
        if (spawnedEnemiesTotal >= currentPack.enemyTotalCount)
            return;

        if (currentActiveEnemies >= currentPack.maxEnemiesAlive)
            return;

        currentActiveEnemies++;
        spawnedEnemiesTotal++;

        EnemyBase enemeyPrefab = currentPack.possibleEnemies.GetRandom();
        EnemyBase newEnemy = GameobjectPoolSystem.Instantiate(enemeyPrefab.gameObject, position, Quaternion.identity, enemyRoot).GetComponent<EnemyBase>();
        newEnemy.name = enemeyPrefab.name;
        newEnemy.onDieEvent += OnEnemyDied;
        newEnemy.Init();
    }

    private void OnEnemyDied(EnemyBase sender)
    {
        sender.onDieEvent -= OnEnemyDied;

        enemiesKilled++;
        currentActiveEnemies--;

        if (enemiesKilled >= currentPack.enemyTotalCount)
        {
            onAllEnemiesDied?.Invoke();
            StopSpawning();
        }
    }

    private void GetSpawnPositions()
    {
        spawnPositions = new Vector2[levelRoot.childCount];

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            Vector3 spawnPosition = levelRoot.GetChild(i).position + Vector3.up * raycastHeight;

            Physics.Raycast(spawnPosition, Vector3.down, out RaycastHit hitInfo, raycastHeight * 2, platformMask, QueryTriggerInteraction.Ignore);

            if (hitInfo.collider != null)
            {
                spawnPosition = hitInfo.point + hitInfo.normal * 0.25f;
            }

            spawnPositions[i] = spawnPosition;
        }
    }

    private IEnumerator CoroutineSpawnEnemies()
    {
        yield return new WaitForSeconds(currentPack.spawnDelay);

        Vector2 position = spawnPositions.GetRandom();

        SpawnEnemy(position);

        coroutine_SpawnEnemies = StartCoroutine(CoroutineSpawnEnemies());
    }

    private void OnEnable()
    {
        levelManager.onLevelCreated += OnLevelCreated;
    }

    private void OnDisable()
    {
        levelManager.onLevelCreated -= OnLevelCreated;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        if(spawnPositions != null)
        {
            foreach (Vector3 position in spawnPositions)
            {
                Gizmos.DrawWireCube(position, Vector3.one * 0.25f);
            }
        }

    }
}