using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    public ShootBehaviour shootBehaviour;
    public PlayerBehaviour playerBehaviour;

    public float transitionDuration;

    public float timeNormal = 1f;
    public float timeSlowed = 0.35f;
    public float timeOnDeath = 0.01f;

    private Coroutine coroutine_Transition;



    private void TransitionToSlowMo()
    {
        if (coroutine_Transition != null)
            StopCoroutine(coroutine_Transition);

        coroutine_Transition = StartCoroutine(CoroutineTransition(timeSlowed, transitionDuration));
    }

    private void TransitionToNormal()
    {
        if (coroutine_Transition != null)
            StopCoroutine(coroutine_Transition);

        coroutine_Transition = StartCoroutine(CoroutineTransition(timeNormal, transitionDuration));
    }

    private void TransitionToDeath()
    {
        if (coroutine_Transition != null)
            StopCoroutine(coroutine_Transition);

        coroutine_Transition = StartCoroutine(CoroutineTransition(timeOnDeath, transitionDuration));
    }

    private IEnumerator CoroutineTransition(float target, float duration)
    {
        float current = Time.timeScale;

        float timer = 0;

        while(timer < duration)
        {
            timer += Time.unscaledDeltaTime;

            Time.timeScale = Mathf.Lerp(current, target, timer / duration);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;

            yield return null;
        }

        Time.timeScale = target;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        coroutine_Transition = null;
    }

    private void OnEnable()
    {
        shootBehaviour.onBeginShootEvent += TransitionToSlowMo;
        shootBehaviour.onEndShootEvent += TransitionToNormal;
        playerBehaviour.onPlayerDied += TransitionToDeath;
    }

    private void OnDisable()
    {
        shootBehaviour.onBeginShootEvent -= TransitionToSlowMo;
        shootBehaviour.onEndShootEvent -= TransitionToNormal;
        playerBehaviour.onPlayerDied -= TransitionToDeath;
    }
}