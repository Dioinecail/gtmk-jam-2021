using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBoundsController : MonoBehaviour
{
    public Vector2 bounds;
    public Vector2 center;

    public float outOfBoundsTeleportOffset;
    public float findEnemiesDelay = 0.2f;

    private Transform player;
    private Transform cameraTransform;
    private Vector3 currentCameraOffset;
    private Transform[] cachedEnemies;
    private Coroutine coroutine_FindEnemies;



    private void Awake()
    {
        player = FindObjectOfType<PlayerBehaviour>().transform;
        cameraTransform = FindObjectOfType<CameraFollower>().transform;

        coroutine_FindEnemies = StartCoroutine(CoroutineFindEnemies());
    }

    private void Update()
    {
        currentCameraOffset = cameraTransform.position - player.position;
        CheckIfOutOfBounds(player, true);

        if(cachedEnemies != null && cachedEnemies.Length > 0)
        {
            for (int i = 0; i < cachedEnemies.Length; i++)
            {
                CheckIfOutOfBounds(cachedEnemies[i], false);
            }
        }
    }

    private void CheckIfOutOfBounds(Transform target, bool player)
    {
        Vector2 targetPosition = new Vector2(target.position.x, target.position.y);

        Vector2 min = center - bounds;
        Vector2 max = center + bounds;

        if(targetPosition.y > max.y)
        {
            // out of upper bound;
            OutOfUpperBound(target, player);
        }
        else if (targetPosition.y < min.y)
        {
            // out of lower bound;
            OutOfLowerBound(target, player);
        }

        if (targetPosition.x > max.x)
        {
            // out of right bound;
            OutOfRightBound(target, player);
        }
        else if (targetPosition.x < min.x)
        {
            // out of left bound;
            OutOfLeftBound(target, player);
        }
    }

    private void OutOfLeftBound(Transform target, bool player)
    {
        Vector3 newPosition = new Vector3((center + bounds).x - outOfBoundsTeleportOffset, target.position.y, target.position.z);
        SetNewPosition(target, newPosition, player);
    }

    private void OutOfRightBound(Transform target, bool player)
    {
        Vector3 newPosition = new Vector3((center - bounds).x + outOfBoundsTeleportOffset, target.position.y, target.position.z);
        SetNewPosition(target, newPosition, player);
    }

    private void OutOfLowerBound(Transform target, bool player)
    {
        Vector3 newPosition = new Vector3(target.position.x, (center + bounds).y - outOfBoundsTeleportOffset, target.position.z);
        SetNewPosition(target, newPosition, player);
    }

    private void OutOfUpperBound(Transform target, bool player)
    {
        Vector3 newPosition = new Vector3(target.position.x, (center - bounds).y + outOfBoundsTeleportOffset, target.position.z);
        SetNewPosition(target, newPosition, player);
    }

    private void SetNewPosition(Transform target, Vector3 newPosition, bool player)
    {
        target.position = newPosition;

        if(player)
            cameraTransform.position = target.position + currentCameraOffset;
    }

    private void FindEnemies()
    {
        EnemyBase[] enemies = FindObjectsOfType<EnemyBase>();

        cachedEnemies = new Transform[enemies.Length];

        for (int i = 0; i < cachedEnemies.Length; i++)
        {
            cachedEnemies[i] = enemies[i].transform;
        }
    }

    private IEnumerator CoroutineFindEnemies()
    {
        yield return new WaitForSeconds(findEnemiesDelay);

        FindEnemies();

        coroutine_FindEnemies = StartCoroutine(CoroutineFindEnemies());
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(center, bounds * 2);
    }
}
