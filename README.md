# SUBMISSION FOR GMTK JAM 2021

![Image](Screenshots/screenshot_0.png)  

Personal Links:  
[Twitter](https://twitter.com/dioinecail/)  
[Itch.io](https://dioinecail.itch.io/)  

Source project used:  
[Beat World](https://dioinecail.itch.io/beat-world)  

Assets:  
[Kenney's platformer kit](https://www.kenney.nl/assets/platformer-kit)  
[Character](https://sketchfab.com/3d-models/low-poly-male-character-free-download-634469944b1f4778940da77b72deb439)  
[Rope physics](https://github.com/dci05049/SlingShotVerletIntegration)  

Animation:  
[Mixamo](https://www.mixamo.com)  

Sound:  
[SFXR](http://sfxr.me)